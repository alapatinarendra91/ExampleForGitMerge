package com.dailywalkin.example

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.AudioManager



class SilenceBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
//        throw UnsupportedOperationException("Not yet implemented")
        val audio = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

        audio.ringerMode = AudioManager.RINGER_MODE_SILENT
    }
}
