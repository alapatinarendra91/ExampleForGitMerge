package com.dailywalkin.example;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.brandongogetap.stickyheaders.StickyLayoutManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.uber.sdk.android.rides.RideParameters;
import com.uber.sdk.android.rides.RideRequestButton;
import com.uber.sdk.android.rides.RideRequestButtonCallback;
import com.uber.sdk.rides.client.ServerTokenSession;
import com.uber.sdk.rides.client.Session;
import com.uber.sdk.rides.client.SessionConfiguration;
import com.uber.sdk.rides.client.UberRidesApi;
import com.uber.sdk.rides.client.error.ApiError;
import com.uber.sdk.rides.client.error.ErrorParser;
import com.uber.sdk.rides.client.model.Product;
import com.uber.sdk.rides.client.model.Ride;
import com.uber.sdk.rides.client.model.RideEstimate;
import com.uber.sdk.rides.client.model.RideRequestParameters;
import com.uber.sdk.rides.client.model.UserProfile;
import com.uber.sdk.rides.client.services.RidesService;

import retrofit2.Response;

/**
 * Created by CIS16 on 31-Jan-18.
 */


public class Main extends AppCompatActivity {
    private RecyclerView recyclerView;
//    private SearchJobsAdapter adapter;
    private RecyclerAdapter adapter;
    ArrayList<SearchJobsModel> mSearchJobsModelList = new ArrayList<>();
    private Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;


       /* // get the context by invoking ``getApplicationContext()``, ``getContext()``, ``getBaseContext()`` or ``this`` when in the activity class
        RideRequestButton requestButton = new RideRequestButton(mContext);
      // get your layout, for instance:
     RelativeLayout layout = (RelativeLayout) findViewById(R.id.activity_main);
        layout.addView(requestButton);

        RideParameters rideParams = new RideParameters.Builder()
                // Optional product_id from /v1/products endpoint (e.g. UberX). If not provided, most cost-efficient product will be used
                .setProductId("KFumIgcJ08U0YDoO3RloU3Wi49wYbw2P")
                // Required for price estimates; lat (Double), lng (Double), nickname (String), formatted address (String) of dropoff location
                .setDropoffLocation(
                        37.775304, -122.417522, "Uber HQ", "1455 Market Street, San Francisco")
                // Required for pickup estimates; lat (Double), lng (Double), nickname (String), formatted address (String) of pickup location
                .setPickupLocation(37.775304, -122.417522, "Uber HQ", "1455 Market Street, San Francisco")
                .build();
// set parameters for the RideRequestButton instance
        requestButton.setRideParameters(rideParams);

        SessionConfiguration config = new SessionConfiguration.Builder()
                // mandatory
                .setClientId("KFumIgcJ08U0YDoO3RloU3Wi49wYbw2P")
                // required for enhanced button features
                .setServerToken("KA.eyJ2ZXJzaW9uIjoyLCJpZCI6ImZxZVpRVWtKUXUyeVQ1ckNWbGhIZUE9PSIsImV4cGlyZXNfYXQiOjE1MjUzNTU1NzIsInBpcGVsaW5lX2tleV9pZCI6Ik1RPT0iLCJwaXBlbGluZV9pZCI6MX0.65sF61VQNoiKFI2I_iV0-1spGZdrXctdVU5_xIrdXLw")
                // required for implicit grant authentication
                .setRedirectUri("https://www.talentzing.com")
                // optional: set sandbox as operating environment
                .setEnvironment(SessionConfiguration.Environment.SANDBOX)
                .build();

        ServerTokenSession session = new ServerTokenSession(config);
        requestButton.setSession(session);

        requestButton.loadRideInformation();

        RideRequestButtonCallback callback = new RideRequestButtonCallback() {

            @Override
            public void onRideInformationLoaded() {
                // react to the displayed estimates
                Log.e("tag","is: onRideInformationLoaded");
            }

            @Override
            public void onError(ApiError apiError) {
                // API error details: /docs/riders/references/api#section-errors
                Log.e("tag","is: apiError");
            }

            @Override
            public void onError(Throwable throwable) {
                // Unexpected error, very likely an IOException
                Log.e("tag","is: throwable");
            }
        };
        requestButton.setCallback(callback);*/
    }



}
