package com.dailywalkin.example;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.brandongogetap.stickyheaders.exposed.StickyHeader;
import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler;

import java.util.ArrayList;
import java.util.List;

import static android.support.v7.widget.RecyclerView.NO_POSITION;
import static android.view.LayoutInflater.from;

final class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyHolder>
        implements StickyHeaderHandler {

    private final ArrayList<SearchJobsModel> data ;
    ArrayList<ChildModel> mSearchJobsModelList = new ArrayList<>();
    private Context mContext;
    private View view;

    public RecyclerAdapter(Context mContext, ArrayList<SearchJobsModel> mSearchJobsModelList) {
        this.mContext = mContext;
        this.data = mSearchJobsModelList;

    }

    @Override
    public RecyclerAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        final MyHolder viewHolder;
        viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapter.MyHolder holder, final int position) {
        SearchJobsModel item = data.get(position);
        holder.titleTextView.setText(item.getName());
        holder.messageTextView.setText(item.getSecond());
        if (item instanceof StickyHeader) {
            holder.itemView.setBackgroundColor(Color.CYAN);
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }
        holder.titleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, holder.titleTextView.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override public int getItemCount() {
        return data.size();
    }

    @Override public int getItemViewType(int position) {
        if (position != 0 && position % 16 == 0) {
            return 1;
        }
        return 0;
    }

    @Override public List<?> getAdapterData() {
        return data;
    }

    private class MyViewHolder extends MyHolder {

        MyViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView messageTextView;

        public MyHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.tv_title);
            messageTextView = (TextView) itemView.findViewById(R.id.tv_message);

        }
    }
}
