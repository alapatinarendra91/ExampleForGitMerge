package com.dailywalkin.example;

import com.brandongogetap.stickyheaders.exposed.StickyHeader;

/**
 * Created by CIS16 on 28-Dec-17.
 */

public class SearchJobsModel  {
    private String name;
    private String second;
    private String third;
    private String fourth;
    private String fifth;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getThird() {
        return third;
    }

    public void setThird(String third) {
        this.third = third;
    }

    public String getFourth() {
        return fourth;
    }

    public void setFourth(String fourth) {
        this.fourth = fourth;
    }

    public String getFifth() {
        return fifth;
    }

    public void setFifth(String fifth) {
        this.fifth = fifth;
    }
}
