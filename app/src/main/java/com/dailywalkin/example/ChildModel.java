package com.dailywalkin.example;

import com.brandongogetap.stickyheaders.exposed.StickyHeader;

/**
 * Created by CIS16 on 28-Dec-17.
 */

public class ChildModel extends SearchJobsModel implements StickyHeader {
    private String nameChild;
    private String secondChild;

    public String getSecondChild() {
        return secondChild;
    }

    public void setSecondChild(String secondChild) {
        this.secondChild = secondChild;
    }

    public String getNameChild() {
        return nameChild;
    }

    public void setNameChild(String nameChild) {
        this.nameChild = nameChild;
    }
}
