package com.dailywalkin.example;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by CIS16 on 28-Dec-17.
 */

public class SearchJobsAdapter extends RecyclerView.Adapter<SearchJobsAdapter.MyHolder> {

    private Context mContext;
    private ArrayList<SearchJobsModel> mSearchJobsModelList;
    private View view;

    /**
     * Constructor provides information about, and access to, a single constructor for a class.
     *
     * @param mContext
     * @param mSearchJobsModelList
     */
    public SearchJobsAdapter(Context mContext, ArrayList<SearchJobsModel> mSearchJobsModelList) {
        this.mContext = mContext;
        this.mSearchJobsModelList = mSearchJobsModelList;

    }

    @Override
    public SearchJobsAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.adapter_search_jobs, null);
        SearchJobsAdapter.MyHolder myHolder = new SearchJobsAdapter.MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(final SearchJobsAdapter.MyHolder holder, final int position) {


        holder.positionNAME.setText(mSearchJobsModelList.get(position).getName());
        holder.compJob.setText(mSearchJobsModelList.get(position).getSecond());


        holder.convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });


    }


    @Override
    public int getItemCount() {
//        return (null != mSearchJobsModelList ? mSearchJobsModelList.size() : 0);
        return mSearchJobsModelList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        private View convertView;
        private TextView positionNAME,compJob;

        public MyHolder(View view) {
            super(view);
            convertView = view;
            positionNAME = view.findViewById(R.id.positionNAME);
            compJob = view.findViewById(R.id.compJob);

        }
    }


}